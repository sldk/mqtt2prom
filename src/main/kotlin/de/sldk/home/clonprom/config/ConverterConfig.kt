package de.sldk.home.clonprom.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@Component
@ConfigurationProperties(prefix = "converter")
class ConverterConfig {

    /*TODO this config must become much more generic*/

    lateinit var configs: List<ConverterMapping>

    class ConverterMapping {
        lateinit var label: String
        lateinit var mappings: Map<String, String>
    }
}