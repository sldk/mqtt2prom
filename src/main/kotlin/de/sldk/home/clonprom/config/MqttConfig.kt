package de.sldk.home.clonprom.config

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.google.common.flogger.FluentLogger
import org.eclipse.paho.client.mqttv3.MqttConnectOptions
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.integration.annotation.ServiceActivator
import org.springframework.integration.annotation.Transformer
import org.springframework.integration.channel.DirectChannel
import org.springframework.integration.core.MessageProducer
import org.springframework.integration.mqtt.core.DefaultMqttPahoClientFactory
import org.springframework.integration.mqtt.inbound.MqttPahoMessageDrivenChannelAdapter
import org.springframework.integration.mqtt.support.DefaultPahoMessageConverter
import org.springframework.integration.support.json.Jackson2JsonObjectMapper
import org.springframework.messaging.Message
import org.springframework.messaging.MessageChannel
import org.springframework.messaging.MessageHandler


@Configuration
class MqttConfig {

    @ConfigurationProperties("mqtt")
    class MqttProperties {
        lateinit var brokerUrl: String
        lateinit var clientId: String
        lateinit var topic: String
        lateinit var username: String
        lateinit var password: String
    }

    @Autowired
    lateinit var mqttProperties: MqttProperties

    @Autowired
    lateinit var messageToRepositoryHandler: MessageToRepositoryHandler

    @Bean
    fun inbound(): MessageProducer {

        val factory = DefaultMqttPahoClientFactory()

        factory.connectionOptions = MqttConnectOptions().apply {
            userName = mqttProperties.username
            password = mqttProperties.password.toCharArray()
        }

        val adapter = MqttPahoMessageDrivenChannelAdapter(mqttProperties.brokerUrl, mqttProperties.clientId,
                factory, mqttProperties.topic)
        adapter.setCompletionTimeout(5000)
        adapter.setConverter(DefaultPahoMessageConverter())
        adapter.setQos(1)
        adapter.outputChannel = mqttInputChannel()
        return adapter
    }

    @Bean
    fun mqttInputChannel(): MessageChannel {
        return DirectChannel()
    }

    @Transformer(inputChannel = "mqttInputChannel", outputChannel = "mqttOutputChannel")
    @ServiceActivator
    fun jsonTransformer(message: Message<*>): JsonNode? {
        val mapper = ObjectMapper().registerKotlinModule()
        val objectMapper = Jackson2JsonObjectMapper(mapper)
        return objectMapper.toJsonNode(message.payload)
    }

    @Bean
    fun mqttOutputChannel(): MessageChannel {
        return DirectChannel()
    }

    @Bean
    @ServiceActivator(inputChannel = "mqttOutputChannel")
    fun handler(): MessageHandler {
        return messageToRepositoryHandler
    }
}

