package de.sldk.home.clonprom

import de.sldk.home.clonprom.config.ConverterConfig
import de.sldk.home.clonprom.config.MqttConfig
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties

@SpringBootApplication
@EnableConfigurationProperties(MqttConfig.MqttProperties::class, ConverterConfig::class)
class Clonprom

fun main(args: Array<String>) {
    SpringApplication.run(Clonprom::class.java, *args)
}